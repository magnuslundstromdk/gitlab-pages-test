import * as React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Welcome to the project"/>
    <h2>About the project</h2>
    <p>This project is a banking website, allowing people to transfer money between their accounts, as well as taking loans.</p>
    <h3>Linting:</h3>
  <p>We are making use of <span style={{color:"red"}}>INSERT LINTER</span> and ....</p>

  <h3>Dockerizing</h3>
  <p>You can run these and these commands in order to get the project up and running</p>
  </Layout>
)

export default IndexPage
